import yaml

"""
读取Yaml的文件
"""


class YamlTool(object):
    def __init__(self) -> None:
        pass

    @staticmethod
    def readData(file_path) -> map:
        # 打开并读取YAML文件
        with open(file_path, 'r', encoding='utf-8') as yaml_file:
            yaml_data = yaml.safe_load(yaml_file)
            return yaml_data
